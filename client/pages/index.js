import styles from "../styles/Home.module.css";
import Header from "../components/Header";
import StakeDetails from "../components/StakeDetails";
import StakeForm from "../components/StakeForm";
import { useMoralis } from "react-moralis";
import { useEffect } from "react";

export default function Home() {
  const { account, isWeb3Enabled } = useMoralis();

  return (
    <main className="bg-gradient-to-r from-zinc-300 to-indigo-600">
      <div className={` ${styles.container}`}>
        <Header />
        <StakeDetails account={account} isWeb3Enabled={isWeb3Enabled}/>
        <StakeForm account={account} isWeb3Enabled={isWeb3Enabled}/>
      </div>
    </main>
  );
}