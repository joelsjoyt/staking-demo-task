import React, { useEffect, useState } from 'react';
import { useMoralis, useWeb3Contract } from 'react-moralis';
import StakingAbi from '../constants/Staking.json';
import TokenAbi from '../constants/RewardToken.json';
import { Form } from 'web3uikit';
import { ethers } from 'ethers';

function StakeForm({account, isWeb3Enabled}) {
  const stakingAddress = "0x5663d1A620eafac861DD6F2c9c457E537EFfa2b9"; //replace this with the address where you have deployed your staking Smart Contract
  const tesTokenAddress = "0x80e9835681dAc09a0c95Ce666A8efF2D8d8d637e"; //replace this with the address where you have deployed your Reward Token Smart Contract
  const [isStaked, setIsStaked] = useState(false)
  const [isWithdrawed, setIsWithdrawed] = useState(false);

  const { runContractFunction } = useWeb3Contract();

  const stakedBalanceOptions = {
    abi: StakingAbi.abi,
    contractAddress: stakingAddress,
    functionName: 'getStaked',
    params: {
      account
    }
  };

  let approveOptions = {
    abi: TokenAbi.abi,
    contractAddress: tesTokenAddress,
    functionName: 'approve'
  };

  let stakeOptions = {
    abi: StakingAbi.abi,
    contractAddress: stakingAddress,
    functionName: 'stake'
  };

  let claimOptions = {
    abi: StakingAbi.abi,
    contractAddress: stakingAddress,
    functionName: 'claimReward'
  }

  let withdrawOptions = {
    abi: StakingAbi.abi,
    contractAddress: stakingAddress,
    functionName: 'withdraw'
  }

  async function getStakedBalance(){
    const stakedBalace = (await runContractFunction({ params:stakedBalanceOptions, onError: (error) => console.log(error) })).toString();
    const formattedStakedBalance = parseFloat(stakedBalace) / 1e18;
    if(formattedStakedBalance > 0){
      setIsStaked(true)
    }
  }

  async function handleStakeSubmit(data) {
    const amountToApprove = data.data[0].inputResult;
    approveOptions.params = {
      amount: ethers.utils.parseEther(amountToApprove, 'ether'),
      spender: stakingAddress
    };

    const tx = await runContractFunction({
      params: approveOptions,
      onError: (error) => console.log(error),
      onSuccess: () => {
        handleApproveSuccess(approveOptions.params.amount);
      }
    });
  }

  async function handleApproveSuccess(amountToStakeFormatted) {
    stakeOptions.params = {
      amount: amountToStakeFormatted
    };

    const tx = await runContractFunction({
      params: stakeOptions,
      onError: (error) => console.log(error)
    });
    setIsStaked(true)
    await tx.wait(0);
    console.log('Stake transaction complete');
  }

  const claimHandler = async () =>{
      const tx = await runContractFunction({
      params: claimOptions,
      onError: (error) => console.log(error)
    });

    const receipt = await tx.wait();
    console.log(receipt); 
    console.log(`Claim transaction complete ast txhash ${receipt.transactionHash}`);
  }

  const handleWithdrawSubmit= async (data) =>{
    const amountToWithdraw = data.data[0].inputResult;
    const amountToWithdrawFormatted = ethers.utils.parseEther(amountToWithdraw, 'ether')
    withdrawOptions.params = {
      amount: amountToWithdrawFormatted,
    };

    const tx = await runContractFunction({
      params:withdrawOptions,
      onError: (error) =>console.log(error.message)
    })

    
    const receipt = await tx.wait();
    setIsWithdrawed(true)
    console.log(`Withdrawal of ${amountToWithdraw} completed ast txHash ${receipt.transactionHash}`);
  }

  useEffect(()=>{
    if(isWeb3Enabled){
      // console.log(isWeb3Enabled);
      getStakedBalance()
      console.log("Is Withdrawed",isWithdrawed);
    }
  },[isStaked, isWeb3Enabled, account, isWithdrawed])
  // console.log(isStaked);
  return (
    <div className='space-y-10'>
    <div className='text-black'>
      <Form
        onSubmit={handleStakeSubmit}
        data={[
          {
            inputWidth: '50%',
            name: 'Amount to stake ',
            type: 'number',
            value: '',
            key: 'amountToStake'
          }
        ]}
        title="Stake Now!"
      ></Form>
    </div>  
    <Form
        onSubmit={handleWithdrawSubmit}
        data={[
          {
            inputWidth: '50%',
            name: 'Amount to withdraw',
            type: 'number',
            value: '',
            key: 'amountToWithdraw'
          }
        ]}
        title="Withdraw"
      ></Form>
    <button onClick={claimHandler} className=" h-20 w-64 bg-blue-500 text-4xl hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
        Claim
    </button>
    
    </div>
    

  );
}

export default StakeForm;
